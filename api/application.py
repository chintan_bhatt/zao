from flask import Flask, jsonify, flash, request
from flask_cors import CORS
import pymysql

application = Flask(__name__)
cors = CORS(application, resources={r"/*": {"origins": "*"}})

from dbConnection import mysql

import time
import logging
import threading
import copy

FileName = "./zao_api_"
FileName += time.strftime("%d-%b-%Y")
FileName += ".log"

with open(FileName,"a+") as f:
    pass

country_data = None
course_level_data = None
language_test_data = None
academic_tests_data = None
origin_country_data = None
all_teaching_language = None
module_data = None
department_data = None
interested_course_data = None
#boards


logging.basicConfig(filename=FileName, level=logging.INFO, format="%(asctime)s - %(levelname)s - "
                                                                  "%(funcName)s - %(message)s - %(lineno)d")

@application.route('/origincountry')
def country():
    logging.info("")
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT country, code from origincountry;")
        origincountries = cur.fetchall()
        data = []
        for cnt in origincountries:
            obj = {}
            obj['label'] = cnt['country']
            obj['value'] = cnt['code']
            data.append(obj)

        resp = jsonify({'code': 0, 'data': data})
        resp.status_code = 200
        return resp

    except Exception as identifier:
        logging.error("{0}".format(identifier))
    finally:
        cur.close()
        conn.close()


def origincountry1():
    global origin_country_data
    logging.info("")
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT country, code from origincountry;")
        origincountries = cur.fetchall()
        data = []
        for cnt in origincountries:
            obj = {}
            obj['label'] = cnt['country']
            obj['value'] = cnt['code']
            data.append(obj)

        origin_country_data = data
    except Exception as identifier:
        logging.error("{0}".format(identifier))
    finally:
        cur.close()
        conn.close()
    logging.info("out")


# get desired course levels
@application.route('/courselevels')
def course_level():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT qualification, qualification_value FROM zao.qualification_level;")
        course_levels = cur.fetchall()

        data = []
        for c in course_levels:
            obj = {}
            obj['label'] = c['qualification_value']
            obj['value'] = c['qualification']
            data.append(obj)
        
        resp = None
        if len(data) <= 0:
            resp = jsonify({'code':1, 'error': "No course devels"})
        else: 
            resp = jsonify({'code':0, 'data': data})
        
        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


def course_level1():
    global course_level_data
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT qualification, qualification_value FROM zao.qualification_level;")
        course_levels = cur.fetchall()

        data = []
        for c in course_levels:
            obj = {}
            obj['label'] = c['qualification_value']
            obj['value'] = c['qualification']
            data.append(obj)

        course_level_data = data[:]
        
    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get language tests name
@application.route('/languagetests')
def language_test():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT test FROM zao.tests where test_type = 'language';")
        language_test = cur.fetchall()
        tests = [test["test"] for test in language_test]
        resp = jsonify({'code': 0, 'name': tests})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


def language_test1():
    global language_test_data
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT test FROM zao.tests where test_type = 'language';")
        language_test = cur.fetchall()
        language_test_data = [test["test"] for test in language_test]


    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get academic tests name
@application.route('/academictests')
def academic_tests():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT test FROM zao.tests where test_type = 'academic';")
        academic_tests = cur.fetchall()
        tests = [test["test"] for test in academic_tests]
        resp = jsonify({'code': 0, 'entrance': tests})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


def academic_tests1():
    global academic_tests_data
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT test FROM zao.tests where test_type = 'academic';")
        academic_tests = cur.fetchall()
        academic_tests_data = [test["test"] for test in academic_tests]

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get ssc board selection name
@application.route('/ssc')
def ssc_board_selection_name():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT board from board;")
        board_list = cur.fetchall()
        boards = [board["board"] for board in board_list]
        resp = jsonify({'code': 0, 'board': boards})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get language of Teachings/Learning
@application.route('/languages')
def learning_and_teaching_languages():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT name FROM zao.language_of_teaching;")
        language_of_teaching_list = cur.fetchall()
        languages = [lan["name"] for lan in language_of_teaching_list]
        resp = jsonify({'code': 0, 'human': languages})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


def learning_and_teaching_languages1():
    global all_teaching_language
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT name FROM zao.language_of_teaching;")
        language_of_teaching_list = cur.fetchall()
        all_teaching_language = [lan["name"] for lan in language_of_teaching_list]

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get interested courses
@application.route('/country/<country_name>/course')
def interested_courses(country_name):
    logging.info("")
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        desired_level = request.args.get("level",None)
        print (desired_level)
        print (desired_level != "")
        print (not desired_level)
        print (desired_level == '""')
        print (country_name)
        print (country_name != "''" and country_name != '""')
        query = ""
        
        if not desired_level or desired_level is None or desired_level == '""':
            query = """ select distinct course.course_id, course.course from course
                inner join course_uni using (course_id)
                inner join university on university.uni_id = course_uni.university_id
                inner join campus on campus.uni_id = university.uni_id
                inner join city on city.city_id = campus.city_id
                inner join states using (state_id)
                inner join country using (country_id)
                where lower(country.country) = lower('""" + country_name + "')"

        elif desired_level and country_name and (country_name != "''" and country_name != '""'):
            query = """ select distinct course.course_id, course.course  from course
                inner join course_uni using (course_id)
                inner join university on university.uni_id = course_uni.university_id
                inner join campus on campus.uni_id = university.uni_id
                inner join city on city.city_id = campus.city_id
                inner join states using (state_id)
                inner join country using (country_id)
                inner join qualification_level 
                on course.course_type = qualification_level.level_id
                where lower(country.country) = lower('""" + country_name + "')" + \
                " and lower(qualification_level.qualification) = LOWER('" + desired_level + "') "
        else:
            # where only desired level is there.
            query = """ select distinct course.course_id, course.course from course
                inner join course_uni using (course_id)
                inner join university on university.uni_id = course_uni.university_id
                inner join campus on campus.uni_id = university.uni_id
                inner join city on city.city_id = campus.city_id
                inner join states using (state_id)
                inner join country using (country_id)
                inner join qualification_level 
                on course.course_type = qualification_level.level_id
                where """ + \
                "  lower(qualification_level.qualification) = LOWER('" + desired_level + "') "
            
        logging.info(query)
        cur.execute(query)
        course_list = cur.fetchall()
        data = []
        for c in course_list:
            obj = {}
            obj['value'] = c['course_id']
            obj['label'] = c['course']
            data.append(obj)
        
        resp = None
        if len(data) <= 0:
            resp = jsonify({'code':1, 'error': "No records found or invalid country or level input"})
        else: 
            resp = jsonify({'code':0, 'data': data})
        
        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


def interested_courses1(country_name, desired_level = None):
    global interested_course_data
    logging.info("")
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        if not desired_level or desired_level is None or desired_level == '""':
            query = """ select distinct  course.course_id, course.course from course
                inner join course_uni using (course_id)
                inner join university on university.uni_id = course_uni.university_id
                inner join campus on campus.uni_id = university.uni_id
                inner join city on city.city_id = campus.city_id
                inner join states using (state_id)
                inner join country using (country_id)
                where lower(country.country) = lower('""" + country_name + "')"

        elif desired_level and country_name and (country_name != "''" and country_name != '""'):
            query = """ select distinct course.course_id, course.course  from course
                inner join course_uni using (course_id)
                inner join university on university.uni_id = course_uni.university_id
                inner join campus on campus.uni_id = university.uni_id
                inner join city on city.city_id = campus.city_id
                inner join states using (state_id)
                inner join country using (country_id)
                inner join qualification_level 
                on course.course_type = qualification_level.level_id
                where lower(country.country) = lower('""" + country_name + "')" + \
                " and lower(qualification_level.qualification) = LOWER('" + desired_level + "') "
        else:
            # where only desired level is there.
            query = """ select distinct course.course_id, course.course from course
                inner join course_uni using (course_id)
                inner join university on university.uni_id = course_uni.university_id
                inner join campus on campus.uni_id = university.uni_id
                inner join city on city.city_id = campus.city_id
                inner join states using (state_id)
                inner join country using (country_id)
                inner join qualification_level 
                on course.course_type = qualification_level.level_id
                where """ + \
                "  lower(qualification_level.qualification) = LOWER('" + desired_level + "') "
        cur.execute(query)
        course_list = cur.fetchall()
        data = []
        for c in course_list:
            obj = {}
            obj['value'] = c['course_id']
            obj['label'] = c['course']
            data.append(obj)
        
        interested_course_data = data

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get department based on countryName
@application.route('/country/<country_name>/department')
def department(country_name):
    logging.info("")
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = """SELECT distinct
                department.department,
                department.department_id
            FROM
                department 
                    INNER JOIN
                course_uni using (department_id) 
                    INNER JOIN
                university ON university.uni_id = course_uni.university_id
                    INNER JOIN
                campus ON campus.uni_id = university.uni_id
                    INNER JOIN
                city ON city.city_id = campus.city_id
                    INNER JOIN
                states USING (state_id)
                    INNER JOIN
                country USING (country_id)
            WHERE
                LOWER(country.country) = LOWER('{country_name}')
                                order BY department
                """
        query = query.format(country_name=country_name)
        cur.execute(query)
        result = cur.fetchall()
        logging.debug("{0}".format(result))
        data = []
        for dept in result:
            obj = {}
            obj['label'] = dept['department']
            obj['value'] = dept['department_id']
            data.append(obj)
        
        resp = None
        if len(data) <= 0:
            resp = jsonify({'code':1, 'error': "please select valid country"})
        else: 
            resp = jsonify({'code':0, 'data': data})
        
        resp.status_code = 200
        return resp

    except Exception as identifier:
        logging.error("{0}".format(identifier))
    finally:
        cur.close()
        conn.close()


def department1(country_name):
    global department_data
    print(country_name)
    logging.info("")
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = """SELECT distinct
                department.department,
                department.department_id
            FROM
                department 
                    INNER JOIN
                course_uni using (department_id) 
                    INNER JOIN
                university ON university.uni_id = course_uni.university_id
                    INNER JOIN
                campus ON campus.uni_id = university.uni_id
                    INNER JOIN
                city ON city.city_id = campus.city_id
                    INNER JOIN
                states USING (state_id)
                    INNER JOIN
                country USING (country_id)
            WHERE
                LOWER(country.country) = LOWER('{country_name}')
                                order BY department"""
        query = query.format(country_name=country_name)

        cur.execute(query)
        result = cur.fetchall()
        logging.debug("{0}".format(result))
        data = []
        for dept in result:
            obj = {}
            obj['label'] = dept['department']
            obj['value'] = dept['department_id']
            data.append(obj)
        department_data = data
       
    except Exception as identifier:
        logging.error("{0}".format(identifier))
    finally:
        cur.close()
        conn.close()
    logging.info("")

# get semester/term
@application.route('/module')
def module():
    # TO_DO may need input param
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        # may need where condition
        cur.execute("SELECT modules FROM zao.modules;")
        module_list = cur.fetchall()
        modules = [module["modules"] for module in module_list]
        resp = jsonify({'code': 0, 'method': modules})
        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()

def module1():
    # TO_DO may need input param
    global module_data
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        # may need where condition
        cur.execute("SELECT modules FROM zao.modules;")
        module_list = cur.fetchall()
        module_data = [module["modules"] for module in module_list]

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# TO-DO check for path param
# get counrty by id, called after filtering/matching
@application.route('/country/<countryID>')
def get_counrty_by_id(countryID):
    # assert countryID == request.view_args['countryID']
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = "SELECT * from country where country_id=" + countryID + ";"
        cur.execute(query)
        country = cur.fetchall()
        if len(country):
            country = country[0]
        resp = jsonify({'code': 0, 'data': country})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get interested country
@application.route('/destinationcountries')
def get_countries():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = "SELECT country, country_name from country;"
        cur.execute(query)
        country_list = cur.fetchall()
        data = []
        for cnt in country_list:
            obj = {}
            obj['value'] = cnt['country']
            obj['label'] = cnt['country_name']
            data.append(obj)

        resp = jsonify({'code': 0, 'data': data})
        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()

def get_countries1():
    global country_data

    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = "SELECT country, country_name from country;"
        cur.execute(query)
        country_list = cur.fetchall()
        data = []
        for cnt in country_list:
            obj = {}
            obj['label'] = cnt['country_name']
            obj['value'] = cnt['country']
            data.append(obj)

        country_data = data

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get interested cities
@application.route('/destinationcities')
def get_cities():
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = "SELECT city from city;"
        cur.execute(query)
        cities_data = cur.fetchall()
        cities = [city["city"] for city in cities_data]
        resp = jsonify({'code': 0, 'city': cities})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get course by id
@application.route('/course/<course_id>')
def get_course_by_id(course_id):
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = "SELECT * from course where course_id=" + course_id + ";"
        cur.execute(query)
        course_data = cur.fetchall()
        if len(course_data):
            course_data = course_data[0]
        course_data['code'] = 0
        resp = jsonify(course_data)

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()

@application.route('/course2/<course_id>')
def get_course2_by_id(course_id):
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = get_search_query3(course_id)
        cur.execute(query)
        course_data = cur.fetchall()
        print (course_data)
        data = []
        
        for cnt in course_data:
            obj = {}
            obj = cnt
            data.append(obj)
        
        resp = None
        if len(course_data) > 0:
            resp = jsonify({'code': 0, 'data': data})
        else:
            resp = jsonify({'code': 1, 'data': 'No data'})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()

# get university by id
@application.route('/university/<university_id>')
def get_university_by_id(university_id):
    try:
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query = "SELECT * from university where uni_id=" + university_id + ";"
        cur.execute(query)
        university_data = cur.fetchall()
        if len(university_data):
            university_data = university_data[0]
        resp = jsonify({'code': 0, 'data': university_data})

        resp.status_code = 200
        return resp

    except Exception as identifier:
        print(identifier)
    finally:
        cur.close()
        conn.close()


# get search result
@application.route('/search', methods=['POST'])
def search():
    logging.info("")
    try:
        param = parse_param(request.json)
        logging.info("param is")
        logging.info(param)
        conn = mysql.connect()
        cur = conn.cursor(pymysql.cursors.DictCursor)
        query, query_without_limit = get_search_query(param, cur)
        
        logging.info(query_without_limit)
        cur.execute(query_without_limit)
        temp_data = cur.fetchall()
        total_records = len(temp_data)
        logging.info('total_records')
        logging.info(total_records)
        
        # min-max budget from result
        budget = []
        budget_set = set()
        for d in temp_data:
            budget_set.add(d['international_fee_usd'])
        
        del temp_data

        logging.info(query)
        cur.execute(query)
        data = cur.fetchall()

        logging.info("Fetch Done")
        search_result = []
        # distinct department from result
        department_list = []
        department_id_set = set()
        # distinct interested course from result
        interested_course = []
        interested_course_list = []
        interested_course_id_set = set()
        interested_course_set = set()
        
        # language of teaching 
        lot = []
        lot_set = set()
        # module
        module = []
        module_set = set()
        # desire_course_level
        lot = []
        lot_set = set()
        desire_course_level = []
        desire_course_level_set = set()
        interested_course_obj = {}

        total_count = len(data)
        
        for d in data:
            sr = {}
            course = {}
            university = {}
            country = {}
            course['course_id'] = d['course_id']
            course['name'] = d['course']
            course['fee'] = d['international_fee_usd']
            
            course['language_of_teaching'] = d['name']
            course['duration'] = d['duration']
            course['foundation'] = d['foundation']
            course['module'] = d['module']
            sr['course'] = course
            # TO-DO create function how much course is matched with input filter
            # course['eligibility_criteria_matched'] =
            university['uni_id'] = d['uni_id']
            university['name'] = d['uni_name']
            university['logo'] = d['logo_url']
            university['state'] = d['state_name']
            university['scholarship'] = d['scholarship']
            university['global_rank'] = d['global_ranking']
            university['employability'] = d['placement_ratio']
            sr['university'] = university

            country['country_short'] = d['country']
            country['name'] = d['country_name']
            country['safety'] = d['safety']
            country['post_study_work'] = d['post_study_work']
            country['immigration_friendly'] = d['Immigration_friendly']
            country['wages_per_hour'] = d['min_wages_phour']
            country['flag_image'] = d['flag_image']
            sr['country'] = country
            search_result.append(sr)
            d_obj = {}
            d_obj['department_id'] = d['department_id']
            d_obj['department'] = d['department']
            department_list.append(d_obj)
            # department_list.append(d_obj)
            department_id_set.add(d['department_id'])

            
            interested_course_obj['course_id'] = d['course_id']
            interested_course_obj['course_name'] = d['course']
            interested_course_list.append(interested_course_obj)

            interested_course_id_set.add(d['course_short_name'])

            desire_course_level_set.add(d['course_type'])
            lot_set.add(d['name'])
            module_set.add(d['module_id'])
            

        logging.debug(search_result)
        logging.info("")

        department_ids = list(department_id_set)
        department = []
        for id in department_ids:
            obj = {}
            obj['value'] = id
            obj['label'] = get_department_name(id, department_list)
            if obj['label'] is not None:
                department.append(obj)
        
        logging.info(department)
    
        logging.info(interested_course_obj)
        for val in desire_course_level_set:
            desire_course_level.append(get_course_type_name(val))
        for val in module_set: 
            module.append(get_module_name(val))
        lot = list(lot_set)

        if bool(budget_set):
            budget.append(min(budget_set))
            budget.append(max(budget_set))
        data = create_search_response(search_result, department, desire_course_level,
            budget, lot, module, param['destination_country'], total_records, param['desire_course_level'])
        
        if total_count > 0:
            status = 'true'
            message = "successfully retrived"
        else:
            status = 'false'
            message = "No data found"

        data['status'] = status
        data['message'] = message
        resp = jsonify(data)
        resp.status_code = 200
        return resp

    except Exception as identifier:
        logging.error("{0}".format(identifier))
    finally:
        cur.close()
        conn.close()


def parse_param(body):
    logging.info(body)
    rb = {}
    rb['destination_country'] = body[
        'destination_country'] if 'destination_country' in body and body[
            'destination_country'] != "" else None
    rb['course_name'] = body['course_name'] if 'course_name' in body and body[
        'course_name'] != "" else None
    rb['department'] = body['department'] if 'department' in body and body[
        'department'] != "" else None
    rb['desire_course_level'] = body[
        'desire_course_level'] if 'desire_course_level' in body and body[
            'desire_course_level'] != "" else None
    rb['language_test'] = body[
        'language_test'] if 'language_test' in body and body[
            'language_test'] != "" else None
    rb['language_overall_score'] = body[
        'language_overall_score'] if 'language_overall_score' in body and body[
            'language_overall_score'] != "" else None
    rb['listening'] = body['listening'] if 'listening' in body and body[
        'listening'] != "" else None
    rb['reading'] = body[
        'reading'] if 'reading' in body and body['reading'] != "" else None
    rb['writing'] = body[
        'writing'] if 'writing' in body and body['writing'] != "" else None
    rb['speaking'] = body[
        'speaking'] if 'speaking' in body and body['speaking'] != "" else None

    # ['GRE', 'GMAT', 'SAT', 'ACT']
    rb['academic_test'] = body['academic_test'] if ('academic_test' in body and
        body['academic_test'] != "") else None
    rb['overall_score'] = body['overall_score'] if ('overall_score' in body and
        body['overall_score'] != "") else None
    
    # for both GRE and GMAT
    rb['verbal'] = body['verbal'] if 'verbal' in body and body['verbal'] != "" else None
    rb['quantitative'] = body['quantitative'] if ('quantitative' in body and
        body['quantitative'] != "") else None
    
    # GMAT
    rb['analytical_writing'] = body['analytical_writing'] if ('analytical_writing' in
        body and body['analytical_writing'] != "") else None
    rb['reasoning'] = body['reasoning'] if ('reasoning' in body and
        body['reasoning'] != "") else None
    # SAT
    rb['evidence_read_write'] = body['evidence_read_write'] if ('evidence_read_write' in
        body and body['evidence_read_write'] != "") else None
    # for both SAT and ACT
    rb['academy_math'] = body['academy_math'] if ('academy_math' in body and
        body['academy_math'] != "") else None

    # ACT
    rb['act_english'] = body['act_english'] if ('act_english' in body and
        body['act_english'] != "") else None
    rb['act_reading'] = body['act_reading'] if ('act_reading' in body and
        body['act_reading'] != "") else None
    rb['act_science'] = body['act_science'] if ('act_science' in body and
        body['act_science'] != "") else None

    rb['origin_country'] = body[
        'origin_country'] if 'origin_country' in body and body[
            'origin_country'] != "" else None
    rb['min_budget'] = body[
        'min_budget'] if 'min_budget' in body and body['min_budget'] != "" else None
    rb['max_budget'] = body[
        'max_budget'] if 'max_budget' in body and body['max_budget'] != "" else None

    rb['board_name'] = body['board_name'] if 'board_name' in body and body[
        'board_name'] != "" else None
    rb['board_percentage'] = body['board_percentage'] if 'board_percentage' in body and body[
        'board_percentage'] != "" else None
    rb['min_grade_board_percentage'] = body['min_grade_board_percentage'] if 'min_grade_board_percentage' in body and body[
        'min_grade_board_percentage'] != "" else None
    rb['max_grade_board_percentage'] = body['max_grade_board_percentage'] if 'max_grade_board_percentage' in body and body[
        'max_grade_board_percentage'] != "" else None

    rb['ssc_board'] = body['ssc_board'] if 'ssc_board' in body and body[
        'ssc_board'] != "" else None
    
    rb['ssc_percentage'] = body[
        'ssc_percentage'] if 'ssc_percentage' in body and body['ssc_percentage'] != "" else None
    rb['hsc_percentage'] = body[
        'hsc_percentage'] if 'hsc_percentage' in body and body['hsc_percentage'] != "" else None
    
    rb['ssc_percentage_min'] = body[
        'ssc_percentage_min'] if 'ssc_percentage_min' in body and body['ssc_percentage_min'] != "" else None
    rb['hsc_percentage_min'] = body[
        'hsc_percentage_min'] if 'hsc_percentage_min' in body and body['hsc_percentage_min'] != "" else None

    rb['ssc_percentage_max'] = body[
        'ssc_percentage_max'] if 'ssc_percentage_max' in body and body['ssc_percentage_max'] != "" else None
    rb['hsc_percentage_max'] = body[
        'hsc_percentage_max'] if 'hsc_percentage_max' in body and body['hsc_percentage_max'] != "" else None

    rb['experience_require'] = body[
        'experience_require'] if 'experience_require' in body and body['experience_require'] != "" else None
    
    rb['bachelor_percentage'] = body[
        'bachelor_percentage'] if 'bachelor_percentage' in body and body[
            'bachelor_percentage'] != "" else None
    rb['master_percentage'] = body['master_percentage'] if 'master_percentage' in body and body[
        'master_percentage'] != "" else None

    rb['bachelor_percentage_min'] = body[
        'bachelor_percentage_min'] if 'bachelor_percentage_min' in body and body[
            'bachelor_percentage_min'] != "" else None
    rb['master_percentage_min'] = body['master_percentage_min'] if 'master_percentage_min' in body and body[
        'master_percentage_min'] != "" else None

    rb['bachelor_percentage_max'] = body[
        'bachelor_percentage_max'] if 'bachelor_percentage_max' in body and body[
            'bachelor_percentage_max'] != "" else None
    rb['master_percentage_max'] = body['master_percentage_max'] if 'master_percentage_max' in body and body[
        'master_percentage_max'] != "" else None

    rb['board_math_marks'] = body[
        'board_math_marks'] if 'board_math_marks' in body and body[
            'board_math_marks'] != "" else None
    rb['board_english_marks'] = body[
        'board_english_marks'] if 'board_english_marks' in body and body[
            'board_english_marks'] != "" else None
    rb['board_science_marks'] = body[
        'board_science_marks'] if 'board_science_marks' in body and body[
            'board_science_marks'] != "" else None
    rb['language_of_teaching'] = body[
        'language_of_teaching'] if 'language_of_teaching' in body and body[
            'language_of_teaching'] != "" else None
    rb['module'] = body[
        'module'] if 'module' in body and body['module'] != "" else None
    rb['page_num'] = body['page_num'] if 'page_num' in body and body['page_num'] != "" else None
    rb['limit'] = body['limit'] if 'limit' in body and body['limit'] != "" else None

    # for key, value in rb.items() :
    #     print(key)
    logging.info("request params filtered below")
    logging.info(rb)
    return rb

def create_origin_country_qualification_criteria_filter(param):
    # not check for diploma
    logging.info(param)
    country = param['origin_country'] if 'origin_country' in param else None
    
    if country is None or country == "":
        logging.info("")
        return {}
    logging.info('country is '+ country)

    count = 0
    
    inner_join_grade_query = """ INNER JOIN (
        SELECT DISTINCT course_id FROM course_country_criteria 
        INNER JOIN origincountry USING (origincountry_id)
        INNER JOIN qualification_level USING (level_id)
        where LOWER(code) = LOWER('{origin_country}') 
        and ( course_country_criteria.min_percent between {min_grade_board_percentage}*(100 - 5) 
            and {max_grade_board_percentage}*(100 + 5)
        or course_country_criteria.min_percent <= {min_grade_board_percentage}*(100 - 5) ) 
        ) AS cmtable ON cmtable.course_id = course.course_id """

    inner_join_grade_part = """ ( (course_country_criteria.min_percent between {min_grade_board_percentage}*(100 - 5) 
            and {max_grade_board_percentage}*(100 + 5)
        or course_country_criteria.min_percent <= {min_grade_board_percentage}*(100 - 5) )
        AND qualification = "{qual}" ) """

    where_clause_list = []
    UG_given =  PG_given = False

    inner_join__part = """ (course_country_criteria.min_percent <= {board_percentage}* (100 + 5)
        AND qualification = "{qual}" ) """
    bach = hsc = ssc = ""
    if param['bachelor_percentage'] is not None:
        bach = inner_join__part.format(board_percentage=param['bachelor_percentage'], qual="PG")
        count = 1 
        PG_given = True
    elif param['bachelor_percentage_min'] is not None and param['bachelor_percentage_max'] is not None:
        bach = inner_join_grade_part.format(
            min_grade_board_percentage=param['bachelor_percentage_min'],
            max_grade_board_percentage=param['bachelor_percentage_max'], qual="PG")
        count = 1
        PG_given = True
    if param['hsc_percentage'] is not None:
        hsc = inner_join__part.format(board_percentage=param['hsc_percentage'], qual="UG")
        count = 1
        UG_given = True
    elif param['hsc_percentage_min'] is not None and param['hsc_percentage_max'] is not None:
        hsc = inner_join_grade_part.format(
            min_grade_board_percentage=param['hsc_percentage_min'],
            max_grade_board_percentage=param['hsc_percentage_max'], qual="UG")
        count = 1
        UG_given = True
    elif param['ssc_percentage'] is not None:
        ssc = inner_join__part.format(board_percentage=param['ssc_percentage'], qual="UG")
        count = 1
        UG_given = True
    elif param['ssc_percentage_min'] is not None and param['ssc_percentage_max'] is not None:
        ssc = inner_join_grade_part.format( 
            min_grade_board_percentage=param['ssc_percentage_min'],
            max_grade_board_percentage=param['ssc_percentage_max'], qual="UG")
        count = 1
        UG_given = True

    logging.info('oring_country_qualification_inner_join query part');
    logging.info( bach + hsc + ssc)
        
    if bach != "" and (hsc != "" or ssc != ""):
        bach = bach + " OR "
    if hsc != "" and ssc != "":
        hsc = hsc + " OR "

    desired_level = ""
    if param['desire_course_level'] != "" and param['desire_course_level'] is not None:
        desired_level = "and qualification = lower('" + param['desire_course_level'] + "') "

    logging.info(desired_level)
    inner_join_query = """
        INNER JOIN
        (
        SELECT DISTINCT course_id
        FROM course_country_criteria 
        INNER JOIN origincountry USING (origincountry_id)
        INNER JOIN qualification_level USING (level_id)
        where LOWER(code) = LOWER('{origin_country}') {desire_level}
        and 
         ( {parts} )
         or ( {param_not_given})
        ) AS cmtable 
        ON cmtable.course_id = course.course_id
        """
    rule1_query = ""
    if not UG_given and PG_given:
        rule1_query = inner_join_query.format(origin_country=country,parts=bach +  hsc + ssc,
            desire_level=desired_level, param_not_given='qualification = "UG"') 

    if not PG_given and UG_given:
        rule1_query = inner_join_query.format(origin_country=country,parts=bach +  hsc + ssc,
            desire_level=desired_level, param_not_given='qualification = "PG"')

    if PG_given and UG_given:
        rule1_query = inner_join_query.format(origin_country=country,parts=bach +  hsc + ssc,
            desire_level=desired_level, param_not_given='" "')

    logging.info('oring_country_qualification_inner_join FinalQuery' + rule1_query)

    if count > 0:
        full = { "inner_query" : rule1_query,
                 "where_clause" : where_clause_list }
        return copy.deepcopy(full)
    else:
        logging.info('oring_country_qualification_inner_join COUNT ZERO FOUND')
        return {}


def create_board_criteria_filter(param):
    board_criteria_inner_query = """INNER JOIN
    (SELECT * FROM course_board_criteria
	INNER JOIN board USING (board_id)
	INNER JOIN origincountry ON
    board.regioncountry_id = origincountry.origincountry_id
    INNER JOIN qualification_level ON
    board.qualification_level_id = qualification_level.level_id
    where LOWER(code) = LOWER('{origin_country}')
    AND course_board_criteria.min_percentage <= {board_percentage}* (100 + 5))
    AS boardTable ON
    boardTable.course_id = course.course_id
    """

    board_grade_criteria_inner_query = """INNER JOIN (
        SELECT *
        FROM course_board_criteria
        INNER JOIN board USING (board_id)
        INNER JOIN origincountry ON
        board.regioncountry_id = origincountry.origincountry_id
        INNER JOIN qualification_level ON
        board.qualification_level_id = qualification_level.level_id

        where LOWER(code) = LOWER('{origin_country}') and (
        course_board_criteria.min_percentage between {min_grade_board_percentage}*(100 - 5) 
            and {max_grade_board_percentage}*(100 + 5)
        or course_board_criteria.min_percentage <= {min_grade_board_percentage}*(100 - 5)) ) AS boardTable ON
        boardTable.course_id = course.course_id
    """

    temp_query = ""
    if param['origin_country'] is not None and param['board_percentage'] is not None:
        temp_query = board_criteria_inner_query.format(origin_country=param['origin_country'], board_percentage=param['board_percentage'])
    elif param['origin_country'] is not None and param['min_grade_board_percentage'] is not None and param['max_grade_board_percentage'] is not None:
        temp_query = board_grade_criteria_inner_query.format(origin_country=param['origin_country'], min_grade_board_percentage=param['min_grade_board_percentage'], max_grade_board_percentage=param['max_grade_board_percentage'])
    return temp_query


def create_search_response(array, department, desire_course_level, budget, lot, module,
    country, total_records, course_level):
    result = {}
    if len(array) > 0:
        result['total_count'] = len(array)
        result['total_records_without_limit'] = total_records
        filter_value = {}
        filter_value['department'] = department
        # filter_value['interested_course'] = interested_course
        filter_value['desire_course_level'] = desire_course_level
        filter_value['teaching_language'] = lot
        filter_value['module'] = module
        filter_value['budget'] = budget
        result['valid_filter_value'] = filter_value
        result['data'] = array

    result['valid_filter_data_value'] = valid_filter_data_value(country, course_level)
    response = {}
    response['result'] = {}
    response['result'] = result

    return response


def get_search_query(param, cur):
    logging.info("")
    
    query = """SELECT
       distinct course.course_id,
       course.module_id,
       (select modules.modules from modules where module_id = course.module_id) as module,
       course.course_type,
       course.course,
       course.course_short_name,
       course.international_fee_usd,
       course.duration,
       course.foundation,
       language_of_teaching.name,
    
       university.uni_name,
       university.uni_id,
       university.logo_url,
       states.state_name,
       course.duration,
       uni_yearly.scholarship, 
       uni_yearly.global_ranking,
       uni_yearly.placement_ratio,
    
       country.flag_image,
       country.country,
       country.country_name,
       country.safety,
       country.post_study_work,
       country.Immigration_friendly,
       country.min_wages_phour,
    
       department.department,
       course_uni.department_id
       FROM
       course
           INNER JOIN
       course_uni USING (course_id)
           INNER JOIN
       university ON university.uni_id = course_uni.university_id
           INNER JOIN
       campus ON campus.uni_id = university.uni_id
           INNER JOIN
       city ON city.city_id = campus.city_id
           INNER JOIN
       states USING (state_id)
           INNER JOIN
       country USING (country_id)
           INNER JOIN
       language_of_teaching USING (language_id)
           INNER JOIN
       uni_yearly on uni_yearly.uni_id = university.uni_id
           INNER JOIN
        department on course_uni.department_id = department.department_id    
       """
    query_w = """SELECT
       distinct course.course_id,
       course.international_fee_usd
       FROM
       course
           INNER JOIN
       course_uni USING (course_id)
           INNER JOIN
       university ON university.uni_id = course_uni.university_id
           INNER JOIN
       campus ON campus.uni_id = university.uni_id
           INNER JOIN
       city ON city.city_id = campus.city_id
           INNER JOIN
       states USING (state_id)
           INNER JOIN
       country USING (country_id)
           INNER JOIN
       language_of_teaching USING (language_id)
           INNER JOIN
       uni_yearly on uni_yearly.uni_id = university.uni_id
           INNER JOIN
        department on course_uni.department_id = department.department_id    
       """
    
    where_clause_list = []
    logging.info("language_inner_query: preparing")
    language_test_inner_query_string = language_test_inner_query(param)
    logging.info("language_inner_query: done, query is ")
    logging.info(language_test_inner_query_string)

    logging.info("academic_test_inner_query: preparing")
    academic_test_inner_query_string = academic_test_inner_query(param)
    logging.info("academic_test_inner_query: done. query is ")
    logging.info(academic_test_inner_query_string)

    country_qualification_query_str = ""
    logging.info("create_origin_country_qualification_criteria_filter: preparing")
    country_qualification_obj = create_origin_country_qualification_criteria_filter(param)
    logging.info("create_origin_country_qualification_criteria_filter: done")
    logging.info(country_qualification_obj)
    
    if 'where_clause' in country_qualification_obj:
        for l in country_qualification_obj['where_clause']:
            where_clause_list.append(l)

    logging.info("country_qualification_query_str: inner_query")
    if 'inner_query' in country_qualification_obj:
        country_qualification_query_str = country_qualification_obj['inner_query']
        logging.info("country_qualification_query_str:" + country_qualification_query_str)
    logging.info("country_qualification_query_str: done")
  
    board_criteria_inner_query = ""
  
    # destination_country where clause
    if param['destination_country'] is not None:
        temp_where = "LOWER(country.country) = LOWER('" + param[
            'destination_country'] + "')"
        where_clause_list.append(temp_where)
    # language_of_teaching where clause    
    if param['language_of_teaching'] is not None:
        temp_where = "language_of_teaching.language_id = (SELECT language_id FROM language_of_teaching WHERE LOWER(language_of_teaching.name) = LOWER('" + param[
            'language_of_teaching'] + "'))"
        where_clause_list.append(temp_where)
    # course_name where clause
    if param['course_name'] is not None:
        temp_where = "course.course_id = " + str(param['course_name']) + " "
        where_clause_list.append(temp_where)
    # module where clause
    if param['module'] is not None:
        temp_where = "course.module_id = (SELECT module_id FROM modules WHERE LOWER(modules.modules) = LOWER('" + param[
            'module'] + "'))"
        where_clause_list.append(temp_where)

    # desire course type where clause
    if param['desire_course_level'] is not None:
        temp_where = "course.course_type = (SELECT level_id FROM qualification_level WHERE LOWER(qualification_level.qualification) = LOWER('" + param[
            'desire_course_level'] + "'))"
        where_clause_list.append(temp_where)

    # department where clause
    if param['department'] is not None:
        temp_where = "course_uni.department_id = " + str(param['department']) + " "
        where_clause_list.append(temp_where)

    # budget where clause
    if param['min_budget'] is not None and param['max_budget'] is not None:
        temp_where = "course.international_fee_usd >= " + str(param['min_budget']) +" and course.international_fee_usd <= " + str(param['max_budget']) + " OR course.international_fee_usd IS NULL "
        where_clause_list.append(temp_where)

    logging.info("page limit")
    limit_query = ""
    if param['page_num'] is not None and param['limit'] is not None:
        count = int (param['limit'])
        page_num = int(param['page_num'])
        offset = (page_num - 1)*count
        limit_query = " LIMIT {offset}, {limit}".format(offset=offset, limit=count)


    logging.info("query collection")
    full_query = ""
    full_query_w = ""
    if (language_test_inner_query_string == "" and academic_test_inner_query_string == "" and
        country_qualification_query_str == ""):
        where_clause = create_where_clause(where_clause_list)
        full_query = query + board_criteria_inner_query + where_clause
        full_query_w = query_w + board_criteria_inner_query + where_clause
    elif (language_test_inner_query_string != "" and academic_test_inner_query_string != "" and
        country_qualification_query_str == ""):
        where_clause = create_where_clause_without_where(where_clause_list)
        full_query = query + board_criteria_inner_query + language_test_inner_query_string + academic_test_inner_query_string + where_clause
        full_query_w = query_w + board_criteria_inner_query + language_test_inner_query_string + academic_test_inner_query_string + where_clause
    elif (language_test_inner_query_string == "" and academic_test_inner_query_string == "" and
        country_qualification_query_str != ""):
        where_clause = create_where_clause_without_where(where_clause_list)
        full_query = query + board_criteria_inner_query + country_qualification_query_str + where_clause
        full_query_w = query_w + board_criteria_inner_query + country_qualification_query_str + where_clause
    else:
        where_clause = create_where_clause_without_where(where_clause_list)
        full_query = query + board_criteria_inner_query + language_test_inner_query_string + academic_test_inner_query_string + country_qualification_query_str + where_clause
        full_query_w = query_w + board_criteria_inner_query + language_test_inner_query_string + academic_test_inner_query_string + country_qualification_query_str + where_clause

    hsc_marks_query =  get_12th_marks_query(param)
    exp_query =  get_exp_req_query(param)    
    final = full_query + hsc_marks_query + exp_query + limit_query
    final_without_limit = full_query_w + hsc_marks_query + exp_query
    logging.info(final)
    return final, final_without_limit


def get_12th_marks_query(param):
    logging.info("")
    math = eng = sci = ""
    if param['board_math_marks'] is not None:
        math = " AND course.maths12th <= round(" + str(param['board_math_marks']) + " * 1.05)"
    if param['board_english_marks'] is not None:
        eng = " AND course.english12th <= round(" + str(param['board_english_marks']) + " * 1.05) "
    if param['board_science_marks'] is not None:
        sci = " AND course.science12th <= round(" + str(param['board_science_marks']) + " * 1.05) "
    
    final = math + eng + sci
    logging.info("Query is " + final)

    return final

def get_exp_req_query(param):
    logging.info("")
    exp = ""
    if param['experience_require'] is not None:
        exp = " AND course.experience_require <= round(" + str(param['experience_require']) + ") "
    
    logging.info("Query is " + exp)

    return exp


def get_search_query3(param):
    query = """SELECT 
                *
                FROM
                course
                    INNER JOIN
                course_uni USING (course_id)
                    INNER JOIN
                university ON university.uni_id = course_uni.university_id
                    INNER JOIN
                campus ON campus.uni_id = university.uni_id
                    INNER JOIN
                city ON city.city_id = campus.city_id
                    INNER JOIN
                states USING (state_id)
                    INNER JOIN
                country USING (country_id)
                    INNER JOIN
                language_of_teaching USING (language_id)
                    INNER JOIN
                uni_yearly ON uni_yearly.uni_id = university.uni_id
                    LEFT JOIN
                course_test_criteria USING (course_id)
                    LEFT JOIN
                test_criteria ON test_criteria.test_criteria_id = course_test_criteria.test_criteria_id
                    LEFT JOIN
                tests ON tests.test_id = test_criteria.test_id
                WHERE
                    course.course_id = {_course__id}
                    """

    query = query.format(_course__id=param )
    logging.info(query)
    return query

# add WHERE, AND in SQL
def create_where_clause(where_clause_list):
    where_clause = ""
    length = len(where_clause_list)
    if length == 0:
        return where_clause
    if length == 1:
        where_clause = " WHERE "
        return where_clause + where_clause_list[0]
    where_clause += " WHERE "
    for l in range(length - 1):
        where_clause += where_clause_list[l]
        where_clause += " and "
    return where_clause + where_clause_list[length - 1]


def create_where_clause_without_where(where_clause_list):
    where_clause = ""
    length = len(where_clause_list)
    if length == 0:
        return where_clause
    if length == 1:
        where_clause = " AND "
        return where_clause + where_clause_list[0]
    where_clause += " AND "
    for l in range(length - 1):
        where_clause += where_clause_list[l]
        where_clause += " AND "
    return where_clause + where_clause_list[length - 1]


def language_test_inner_query(param):
    """
        params,                         student provide overall, student provide overall & all
        
        uni provide overall             match overall,          match overall only    
        uni provide overall & all       match overall,          match all
    """
    logging.info("")
    language_query = ""
    if param['language_test'] is None or param['language_test'] == "":
        return language_query
    # test comparions will always be with IELTS.
    # language_test may have PTE/TOFEL, but data will be compared to IELTS- olny
    language_overall_score = """
        INNER JOIN
                ( SELECT *
                FROM
                    test_criteria
                        INNER JOIN
                    tests USING (test_id)
                        INNER JOIN
                    course_test_criteria USING (test_criteria_id)
                WHERE
                    test_type = 'language'
                        AND test = 'IELTS'
                        AND test_subject = 'language_overall_score'
                        AND marks <= {language_overall_score} * 100) as lang_test
                        ON lang_test.course_id = course.course_id"""
   
    if param['language_test'] is not None and param['language_overall_score'] is not None:
        language_query = language_overall_score.format(language_overall_score=param['language_overall_score'])
        
    language_all_score = """
        INNER JOIN ( SELECT course_id
        FROM (SELECT * FROM  (SELECT *, IFNULL(marks, 0) AS marks2
        FROM test_criteria
        INNER JOIN tests USING (test_id)
        LEFT JOIN course_test_criteria USING (test_criteria_id)
        WHERE test_type = 'language' AND test = "IELTS" ) AS localT
        WHERE ( ( test_subject = 'language_overall_score' AND marks2 <= {language_overall_score} * 100 )
        OR ( test_subject = 'listening'  AND marks2 <= {listening} * 100  )
        OR ( test_subject = 'writing'  AND marks2 <= {writing} * 100  )
        OR ( test_subject = 'speaking' AND marks2 <= {speaking} * 100 )
        OR ( test_subject = 'reading' AND marks2 <=  {reading} * 100  ) )  
        ) AS localT2
        GROUP BY course_id HAVING COUNT(*) = 5 {union_query} )  as lang_test
        On lang_test.course_id = course.course_id"""

    union_query = """ UNION SELECT course_id  from ( SELECT *
        FROM test_criteria
                INNER JOIN  
            tests USING (test_id)
                INNER JOIN
            course_test_criteria USING (test_criteria_id)
        WHERE test_type = 'language' AND LOWER(test) = LOWER('IELTS')
            group by course_id  having count(test_subject) = 1 ) as t
            where ( LOWER(test_subject) = LOWER('language_overall_score')
            AND marks <= {overall_score} * 100 )
            group by course_id  having count(*) = 1 """
    
    if (param['language_test'] is not None and param['listening'] is not None and
        param['reading'] is not None and  param['writing'] is not None and 
        param['speaking'] is not None):
        
        language_overall_score = param['language_overall_score']
        listening = param['listening']
        reading = param['reading']
        writing = param['writing']
        speaking = param['speaking']
        union_query = union_query.format(overall_score=language_overall_score)
        language_query = language_all_score.format(language_overall_score=language_overall_score,
            listening=listening,reading=reading,writing=writing,speaking=speaking,
            union_query=union_query.format(overall_score=param['language_overall_score']) )

    return language_query


def academic_test_inner_query(param):
    """
        params,                         student provide overall, student provide overall & all
        
        uni provide overall             match overall,          match overall only    
        uni provide overall & all       match overall,          match all
    """
    logging.info("")
    if param['academic_test'] is None and param['overall_score'] is None:
        return ""
    
    academic_test_overall_score = """
        INNER JOIN
            ( SELECT *
            FROM
                test_criteria
                    INNER JOIN  
                tests USING (test_id)
                    INNER JOIN
                course_test_criteria USING (test_criteria_id)
            WHERE
                test_type = 'academic'
                AND LOWER(test) = LOWER('{test_name}')
                AND LOWER(test_subject) = LOWER('overall_score')
                AND marks <= {academic_overall_score} * 100) as academic_test
                ON academic_test.course_id = course.course_id"""

    academic_query = ""
    if param['academic_test'] is not None and param['overall_score'] is not None:
        academic_query = academic_test_overall_score.format(
            test_name=param['academic_test'], academic_overall_score=param['overall_score'])

            
    academic_test_all_score = """
        INNER JOIN ( SELECT course_id 
        FROM (SELECT * FROM  (SELECT *, IFNULL(marks, 0) AS marks2
        FROM test_criteria
        INNER JOIN tests USING (test_id)
        LEFT JOIN course_test_criteria USING (test_criteria_id)
        WHERE test_type = 'academic' AND LOWER(test) = LOWER('{test_name}') ) AS localT
        WHERE {all__sub_conditions} ) as localT2 
        GROUP BY course_id HAVING COUNT(*) = {count} {union_query} )  as academic_test
        On academic_test.course_id = course.course_id"""

    union_query = """ UNION SELECT course_id  from ( SELECT *
        FROM test_criteria
                INNER JOIN  
            tests USING (test_id)
                INNER JOIN
            course_test_criteria USING (test_criteria_id)
        WHERE test_type = 'academic' AND LOWER(test) = LOWER('{test_name}')
            group by course_id  having count(test_subject) = 1 ) as t
            where ( LOWER(test_subject) = LOWER('overall_score')
            AND marks <= {overall_score} * 100 )
            group by course_id  having count(*) = 1 """
    
    logging.info("")
    logging.info(academic_query)

    if (param['academic_test'].lower() == "gre" and param['verbal'] is not None and
        param['quantitative'] is not None):
        condition = """
            ( ( test_subject = 'overall_score' AND marks2 <= {academic_overall_score} * 100 )
            OR ( test_subject = 'verbal'  AND marks2 <= {verbal_reasoning_marks} * 100  )
            OR ( test_subject = 'quantitative'  AND marks2 <= {quantitative_reasoning_marks} * 100  )
            )""".format(academic_overall_score=param['overall_score'], verbal_reasoning_marks=param['verbal'],
                quantitative_reasoning_marks=param['quantitative'])
        
        academic_query = academic_test_all_score.format(test_name=param['academic_test'],
            all__sub_conditions= condition, count= 3, union_query=union_query.format(
            test_name=param['academic_test'], overall_score=param['overall_score']))
         
    elif (param['academic_test'].lower() == "gmat" and param['verbal'] is not None and
        param['quantitative'] is not None and param['analytical_writing'] is not None and
        param['reasoning'] is not None):
        
        condition = """
            ( ( test_subject = 'overall_score' AND marks2 <= {overall_score} * 100 )
            OR ( test_subject = 'verbal'  AND marks2 <= {verbal} * 100  )
            OR ( test_subject = 'quantitative'  AND marks2 <= {quantitative} * 100  )
            OR ( test_subject = 'analytical_writing'  AND marks2 <= {analytical_writing} * 100  )
            OR ( test_subject = 'reasoning'  AND marks2 <= {reasoning} * 100  )
            )""".format(overall_score=param['overall_score'], verbal=param['verbal'],
                quantitative=param['quantitative'], analytical_writing=param['analytical_writing'],
                reasoning=param['reasoning'])
            
        academic_query = academic_test_all_score.format(test_name= param['academic_test'],
                all__sub_conditions= condition, count= 5, union_query=union_query.format(
                    test_name=param['academic_test'], overall_score=param['overall_score']))

    elif (param['academic_test'].lower() == "sat" and param['evidence_read_write'] is not None and
        param['academy_math'] is not None):
        condition = """
            ( ( test_subject = 'overall_score' AND marks2 <= {overall_score} * 100 )
            OR ( test_subject = 'evidence_read_write'  AND marks2 <= {evidence_read_write} * 100  )
            OR ( test_subject = 'academy_math'  AND marks2 <= {academy_math} * 100  )
            )""".format(overall_score=param['overall_score'], evidence_read_write=param['evidence_read_write'],
            academy_math=param['academy_math'])

        academic_query = academic_test_all_score.format(test_name= param['academic_test'],
                all__sub_conditions= condition, count= 3, union_query=union_query.format(
                    test_name=param['academic_test'], overall_score=param['overall_score']))
        
    elif (param['academic_test'].lower() == "act" and param['act_english'] is not None and
        param['academy_math'] is not None and param['act_reading'] and param['act_science'] is not None):    
        condition = """
            ( ( test_subject = 'overall_score' AND marks2 <= {overall_score} * 100 )
            OR ( test_subject = 'act_english'  AND marks2 <= {act_english} * 100  )
            OR ( test_subject = 'academy_math'  AND marks2 <= {academy_math} * 100 )
            OR ( test_subject = 'act_reading'  AND marks2 <= {act_reading} * 100 )
            OR ( test_subject = 'act_science'  AND marks2 <= {act_science} * 100 )
            )""".format(overall_score=param['overall_score'], act_english=param['act_english'],
                academy_math=param['academy_math'], act_reading=param['act_reading'],
                act_science=param['act_science'])

        academic_query = academic_test_all_score.format(test_name= param['academic_test'],
                all__sub_conditions= condition, count= 5, union_query=union_query.format(
                    test_name=param['academic_test'], overall_score=param['overall_score']))

    logging.info(academic_query)           
    return academic_query

# add WHERE, AND in SQL
def create_where_clause_for_language_academic_test(where_clause_list):
    where_clause = ""
    length = len(where_clause_list)
    if length == 0:
        return where_clause
    if length == 1:
        return where_clause + where_clause_list[0]
    for l in range(length - 1):
        where_clause += where_clause_list[l]
        where_clause += " OR "
    return where_clause + where_clause_list[length - 1]


def create_test_type_clause_for_language_academic_test(test_clause_list):
    where_clause = ""
    length = len(test_clause_list)
    if length == 0:
        return where_clause
    if length == 1:
        return where_clause + test_clause_list[0] + " AND "
    for l in range(length -1):
        where_clause += test_clause_list[l]
        where_clause += " OR "
    return where_clause + test_clause_list[length - 1] + " AND "


def tolerance_marks():
    return ""

# TODO: remove this method and load from the DB
def get_course_type_name(course_type):
    if course_type == 2:
        return "UG"
    elif course_type == 1:
        return "PG"


def get_module_name(module_id):
    if module_id == 1:
        return "Semester"
    elif module_id == 2:
        return "Term"


def valid_filter_data_value(country_name, course_level):
    logging.info("XXXXXXXXXXXXx")
    global country_data
    global course_level_data
    global language_test_data
    global academic_tests_data
    global origin_country_data
    global all_teaching_language
    global module_data
    global department_data
    global interested_course_data

    country_data = None
    course_level_data = None
    language_test_data = None
    academic_tests_data = None
    origin_country_data = None
    all_teaching_language = None
    module_data = None
    department_data = None
    
    country_ = threading.Thread(target=get_countries1)
    country_.start()
    
    all_desire_course_level = threading.Thread(target=course_level1)
    all_desire_course_level.start()

    language = threading.Thread(target=language_test1)
    language.start()

    academic = threading.Thread(target=academic_tests1)
    academic.start()
    
    # origin_country_ = threading.Thread(target=origincountry1)
    # origin_country_.start()
    origincountry1()
    logging.info("origincountry1 called ")
    all_teaching_language_ = threading.Thread(target=learning_and_teaching_languages1)
    all_teaching_language_.start()

    all_module = threading.Thread(target=module1)
    all_module.start()
    logging.info("module1 called ")
    #dept = threading.Thread(target=department1, args=(country_name))
    #dept.start()
    department1(country_name)
    interested_courses1(country_name, course_level)
    logging.info("origXXXXXXXxx ")

    country_.join()
    logging.info("orig 12")
    # origin_country_.join()
    all_module.join()
    all_desire_course_level.join()
    # dept.join()
    logging.info("orig 123")
    all_teaching_language_.join()
    language.join()
    academic.join()
    
    logging.info("orig ")
    print( country_data)
    print( course_level_data)
    print( language_test_data)
    print( academic_tests_data)
    print( origin_country_data)
    print( all_teaching_language)
    print( module_data)
    print( department_data)
    #resp = get_countries1();
    logging.info("orig ")
    resp = {}
    resp["country"] = country_data
    resp["all_desire_course_level"] = course_level_data
    # todo remove functions as well
    # resp["language"] = language_test_data
    # resp["academic_language"] = academic_tests_data
    resp["origin_country"] = origin_country_data
    resp["all_teaching_language"] = all_teaching_language
    resp["all_module"] = module_data
    resp["interested_course"] = interested_course_data
    resp["all_department"] = department_data

    #  resp2 = jsonify({'valid_filter_data_value':{'country':country_data}}) #, "all_desire_course_level": course_level_data, "language": language_test_data, "academic_language":academic_tests_data,"origin_country":origin_country_data,"all_teaching_language": all_teaching_language, "all_module":module_data}})
    # logging.info (resp)
    return resp


def get_department_name(department_id, department_list):
    for dept in department_list:
        if department_id == dept['department_id']:
            return dept['department']
    return None


def get_qualifiacation_name_by_id(id, qualification_data):
    for qual in qualification_data:
        if id == qual['level_id']:
            return qual['qualification_value']
    return None


def get_course_name_by_shortname(shortname, course_data):
    for course in course_data:
        if shortname == course['course_short_name']:
            return course['course_name']
    return None


if __name__ == "__main__":
    application.run(debug=True, use_reloader=False)