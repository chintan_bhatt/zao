-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.26-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for zao
DROP DATABASE IF EXISTS `zao`;
CREATE DATABASE IF NOT EXISTS `zao` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `zao`;

-- Dumping structure for table zao.board
DROP TABLE IF EXISTS `board`;
CREATE TABLE IF NOT EXISTS `board` (
  `board_id` int(11) NOT NULL,
  `board` varchar(40) NOT NULL,
  `regioncountry_id` int(11) NOT NULL,
  `qualification_level_id` int(11) NOT NULL,
  PRIMARY KEY (`board_id`),
  KEY `fk_qualification_level_board` (`qualification_level_id`),
  KEY `fk_regioncountry_board` (`regioncountry_id`),
  CONSTRAINT `fk_qualification_level_board` FOREIGN KEY (`qualification_level_id`) REFERENCES `qualification_level` (`level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_regioncountry_board` FOREIGN KEY (`regioncountry_id`) REFERENCES `origincountry` (`origincountry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.board: ~17 rows (approximately)
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
INSERT IGNORE INTO `board` (`board_id`, `board`, `regioncountry_id`, `qualification_level_id`) VALUES
	(1, 'International baccalaureate', 0, 0),
	(2, 'Irish Level', 0, 0),
	(3, 'American board', 0, 0),
	(4, 'A-Level', 0, 0),
	(5, 'UCAS Tarrif Point', 0, 0),
	(6, 'GCSE', 1, 1),
	(7, 'USA- A to F', 10, 1),
	(8, 'Canada- A to F', 10, 2),
	(9, 'Aus- HD to N', 0, 0),
	(10, 'UK &IR- First-2:2', 0, 0),
	(11, 'UK &IR- First class- Third class', 0, 0),
	(12, 'NZ- A+ to F', 0, 0),
	(13, 'GPA 4 points', 0, 0),
	(14, 'Percent', 0, 0),
	(15, 'GPA 10 points', 0, 0),
	(16, 'India- CBSE- A1toE2', 1, 2),
	(17, 'India- School-AA to F', 1, 2);
/*!40000 ALTER TABLE `board` ENABLE KEYS */;

-- Dumping structure for table zao.campus
DROP TABLE IF EXISTS `campus`;
CREATE TABLE IF NOT EXISTS `campus` (
  `campus_id` int(11) NOT NULL,
  `uni_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `address` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`campus_id`),
  KEY `fk_uni_campus` (`uni_id`),
  KEY `fk_city_campus` (`city_id`),
  CONSTRAINT `fk_city_campus` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_uni_campus` FOREIGN KEY (`uni_id`) REFERENCES `university` (`uni_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.campus: ~7 rows (approximately)
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT IGNORE INTO `campus` (`campus_id`, `uni_id`, `city_id`, `address`) VALUES
	(1, 1, 7, ''),
	(2, 2, 6, ''),
	(3, 3, 5, ''),
	(4, 4, 4, ''),
	(5, 5, 3, ''),
	(6, 6, 2, ''),
	(7, 7, 1, '');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;

-- Dumping structure for table zao.city
DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `fk_country_city` (`state_id`),
  CONSTRAINT `fk_country_city` FOREIGN KEY (`state_id`) REFERENCES `states` (`state_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.city: ~7 rows (approximately)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT IGNORE INTO `city` (`city_id`, `city`, `state_id`) VALUES
	(1, 'Ahmedabad', 1),
	(2, 'Mumbai', 2),
	(3, 'london', 3),
	(4, 'shire', 5),
	(5, 'New York', 4),
	(6, 'Las Vegas', 6),
	(7, 'San Jose', 7);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

-- Dumping structure for table zao.country
DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL,
  `country` varchar(25) DEFAULT NULL,
  `safety` enum('1','2','3','4','5') DEFAULT NULL,
  `political_impact` tinyint(4) DEFAULT NULL,
  `Immigration_friendly` tinyint(1) DEFAULT NULL,
  `post_study_work` tinyint(1) DEFAULT NULL,
  `post_study_work_duration_months` int(11) DEFAULT NULL,
  `part_time_work` tinyint(1) DEFAULT NULL,
  `avg_term_income` int(11) DEFAULT NULL,
  `avg_grad_sal` int(11) DEFAULT NULL,
  `min_wages_phour` int(11) DEFAULT NULL,
  `job_on_grad` tinyint(1) DEFAULT NULL,
  `occupation` varchar(30) DEFAULT NULL,
  `special_industry` varchar(50) DEFAULT NULL,
  `dependent_allowed` tinyint(1) DEFAULT NULL,
  `visa_sucss_rate_id` int(11) DEFAULT NULL,
  `living_expenses_id` int(11) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `time_diff_ind_mins` int(11) DEFAULT NULL,
  `flag_image` varchar(100) DEFAULT NULL,
  `country_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.country: ~3 rows (approximately)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT IGNORE INTO `country` (`country_id`, `country`, `safety`, `political_impact`, `Immigration_friendly`, `post_study_work`, `post_study_work_duration_months`, `part_time_work`, `avg_term_income`, `avg_grad_sal`, `min_wages_phour`, `job_on_grad`, `occupation`, `special_industry`, `dependent_allowed`, `visa_sucss_rate_id`, `living_expenses_id`, `currency`, `time_diff_ind_mins`, `flag_image`, `country_name`) VALUES
	(1, 'US', '3', 1, 1, 1, 10, 1, 345, 4353, 5, 0, 'engineer', 'software', 1, 1, 1, 'USD', 480, 'https://sonna.so/en/wp-content/uploads/2019/07/flag-square-250.png', 'Unites States'),
	(2, 'UK', '5', 1, 1, 1, 11, 1, 232, 4353, 2, 1, 'eng', 's', 1, 1, 1, 'EURO', 300, 'https://images-na.ssl-images-amazon.com/images/I/918Fgy38EcL._AC_UL300_SR300,300_.jpg', 'United Kingdom'),
	(3, 'IN', '5', 1, 1, 1, 11, 1, 232, 4353, 2, 1, 'job', 's', 1, 1, 1, 'EURO', 300, 'https://theflagcompany.in/wp-content/uploads/2018/10/india-flag-small-300x300.png', 'India');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;

-- Dumping structure for table zao.course
DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) NOT NULL,
  `course` varchar(25) NOT NULL,
  `course_type` int(11) NOT NULL,
  `domestic_fee_usd` int(11) DEFAULT NULL,
  `international_fee_usd` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `start_month` varchar(4) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `course_short_name` varchar(45) DEFAULT NULL,
  `maths12th` int(11) NOT NULL DEFAULT '0',
  `english12th` int(11) NOT NULL DEFAULT '0',
  `science12th` int(11) NOT NULL DEFAULT '0',
  `foundation` enum('0','1') NOT NULL DEFAULT '0',
  `experience_require` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`course_id`),
  KEY `fk_qualification_level_course` (`course_type`),
  KEY `fk_language_of_teaching_course` (`language_id`),
  KEY `fk_modules_course` (`module_id`),
  CONSTRAINT `fk_language_of_teaching_course` FOREIGN KEY (`language_id`) REFERENCES `language_of_teaching` (`language_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_modules_course` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_qualification_level_course` FOREIGN KEY (`course_type`) REFERENCES `qualification_level` (`level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.course: ~13 rows (approximately)
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT IGNORE INTO `course` (`course_id`, `course`, `course_type`, `domestic_fee_usd`, `international_fee_usd`, `module_id`, `duration`, `start_month`, `language_id`, `course_short_name`, `maths12th`, `english12th`, `science12th`, `foundation`, `experience_require`) VALUES
	(1, 'Master Of Computer Applic', 2, 100, 200, 1, 36, 'jun', 1, 'MCA', 0, 0, 0, '0', 2),
	(2, 'Bachelor Of Computer Appl', 1, 100, 200, 2, 36, 'jun', 2, 'BCA', 0, 0, 90, '0', 0),
	(3, 'Charted Account', 1, 6950, 6950, 1, 48, 'jul', 1, 'CA', 0, 0, 0, '0', 0),
	(4, 'Master Of Business Admini', 2, 15950, 15950, 1, 12, 'feb', 1, 'MBA', 80, 0, 0, '0', 5),
	(5, 'Bachelor Of Business Admi', 1, 9250, 9250, 1, 24, 'mar', 2, 'BBA', 0, 0, 0, '0', 0),
	(6, 'Bachelor Of Arts', 1, 9250, 9250, 1, 12, 'jul', 1, 'BA', 0, 0, 0, '0', 0),
	(7, 'Master Of Arts', 2, 9250, 9250, 1, 36, 'aug', 1, 'MA', 0, 75, 90, '0', 3),
	(8, 'Bachelor Of Technology', 1, 100, 200, 1, 48, 'jul', 1, 'BTECH', 0, 0, 0, '0', 0),
	(9, 'Bachelor Of Technology', 1, 50, 150, 1, 48, 'jul', 1, 'BTECH', 0, 0, 0, '0', 0),
	(10, 'Master Of Technology', 2, 5000, 4500, 2, 48, 'jul', 1, 'MTECH', 99, 80, 88, '0', 2),
	(11, 'Diploma CE', 1, 5000, 7000, 1, 36, 'jul', 1, 'DCE', 0, 0, 0, '0', 0),
	(12, 'Agriculter', 1, 5500, 5500, 1, 48, 'jul', 1, 'Agri', 0, 0, 60, '0', 0),
	(13, 'Diploma Pharmacy', 1, 2400, 2400, 1, 48, 'jul', 1, 'DPHARM', 0, 0, 0, '0', 0);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- Dumping structure for table zao.course_board_criteria
DROP TABLE IF EXISTS `course_board_criteria`;
CREATE TABLE IF NOT EXISTS `course_board_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_id` int(11) NOT NULL,
  `marks` int(11) NOT NULL,
  `subject_name` varchar(55) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `min_percentage` int(11) DEFAULT NULL,
  `max_percentage` int(11) DEFAULT NULL,
  `percentage_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_board_criteria` (`board_id`),
  KEY `fk_course_board_criteria_course1` (`course_id`),
  CONSTRAINT `fk_course_board_criteria` FOREIGN KEY (`board_id`) REFERENCES `board` (`board_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_board_criteria_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table zao.course_board_criteria: ~6 rows (approximately)
/*!40000 ALTER TABLE `course_board_criteria` DISABLE KEYS */;
INSERT IGNORE INTO `course_board_criteria` (`id`, `board_id`, `marks`, `subject_name`, `course_id`, `min_percentage`, `max_percentage`, `percentage_type`) VALUES
	(1, 8, 5500, 'MAIN', 12, 6000, 8000, '0'),
	(2, 6, 5000, 'MAIN', 6, 8000, 10000, '1'),
	(3, 6, 4000, 'MAIN', 13, 7000, 8500, '1'),
	(4, 2, 5500, 'MAIN', 2, 8000, 9000, '0'),
	(5, 16, 500, 'MAIN', 9, 7000, 10000, '1'),
	(6, 16, 0, NULL, 3, 6500, 10000, NULL);
/*!40000 ALTER TABLE `course_board_criteria` ENABLE KEYS */;

-- Dumping structure for table zao.course_country_criteria
DROP TABLE IF EXISTS `course_country_criteria`;
CREATE TABLE IF NOT EXISTS `course_country_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origincountry_id` int(11) NOT NULL,
  `min_percent` int(11) NOT NULL,
  `max_percent` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_country_criteria` (`origincountry_id`),
  KEY `fk_course_country_criteria_course1` (`course_id`),
  KEY `fk_qualification_level_course_country_criteria` (`level_id`),
  CONSTRAINT `fk_course_country_criteria` FOREIGN KEY (`origincountry_id`) REFERENCES `origincountry` (`origincountry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_country_criteria_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_qualification_level_course_country_criteria` FOREIGN KEY (`level_id`) REFERENCES `qualification_level` (`level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table zao.course_country_criteria: ~6 rows (approximately)
/*!40000 ALTER TABLE `course_country_criteria` DISABLE KEYS */;
INSERT IGNORE INTO `course_country_criteria` (`id`, `origincountry_id`, `min_percent`, `max_percent`, `course_id`, `level_id`) VALUES
	(1, 1, 4500, 6000, 8, 1),
	(2, 1, 6000, 9000, 10, 2),
	(3, 1, 7000, 10000, 9, 1),
	(4, 1, 5500, 10000, 11, 1),
	(5, 1, 6000, 8000, 6, 1),
	(6, 1, 5000, 10000, 7, 2);
/*!40000 ALTER TABLE `course_country_criteria` ENABLE KEYS */;

-- Dumping structure for table zao.course_qualification_criteria
DROP TABLE IF EXISTS `course_qualification_criteria`;
CREATE TABLE IF NOT EXISTS `course_qualification_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualification_criteria_id` int(11) NOT NULL,
  `marks` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_qualification_criteria` (`qualification_criteria_id`),
  CONSTRAINT `fk_course_qualification_criteria` FOREIGN KEY (`qualification_criteria_id`) REFERENCES `qualification_criteria` (`qualification_criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.course_qualification_criteria: ~0 rows (approximately)
/*!40000 ALTER TABLE `course_qualification_criteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_qualification_criteria` ENABLE KEYS */;

-- Dumping structure for table zao.course_test_criteria
DROP TABLE IF EXISTS `course_test_criteria`;
CREATE TABLE IF NOT EXISTS `course_test_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_criteria_id` int(11) NOT NULL,
  `marks` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_test_criteria` (`test_criteria_id`),
  KEY `fk_course_test_criteria_course1` (`course_id`),
  CONSTRAINT `fk_course_test_criteria` FOREIGN KEY (`test_criteria_id`) REFERENCES `test_criteria` (`test_criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_course_test_criteria_course1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table zao.course_test_criteria: ~13 rows (approximately)
/*!40000 ALTER TABLE `course_test_criteria` DISABLE KEYS */;
INSERT IGNORE INTO `course_test_criteria` (`id`, `test_criteria_id`, `marks`, `course_id`) VALUES
	(1, 15, 450, 9),
	(2, 1, 550, 2),
	(3, 3, 600, 2),
	(4, 9, 500, 8),
	(5, 4, 400, 9),
	(6, 10, 700, 8),
	(7, 11, 650, 8),
	(8, 12, 620, 8),
	(9, 13, 580, 8),
	(10, 16, 440, 9),
	(11, 17, 550, 9),
	(12, 15, 880, 6),
	(13, 14, 200, 9);
/*!40000 ALTER TABLE `course_test_criteria` ENABLE KEYS */;

-- Dumping structure for table zao.course_uni
DROP TABLE IF EXISTS `course_uni`;
CREATE TABLE IF NOT EXISTS `course_uni` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `university_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_course_uni` (`course_id`),
  KEY `fk_university_course_uni` (`university_id`),
  KEY `fk_department_course_uni` (`department_id`),
  CONSTRAINT `fk_course_course_uni` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_department_course_uni` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_university_course_uni` FOREIGN KEY (`university_id`) REFERENCES `university` (`uni_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.course_uni: ~13 rows (approximately)
/*!40000 ALTER TABLE `course_uni` DISABLE KEYS */;
INSERT IGNORE INTO `course_uni` (`id`, `course_id`, `university_id`, `department_id`) VALUES
	(1, 1, 1, 1),
	(2, 2, 2, 1),
	(3, 3, 3, 2),
	(4, 4, 4, 3),
	(5, 5, 5, 3),
	(6, 6, 6, 4),
	(7, 7, 7, 4),
	(8, 8, 7, 5),
	(9, 9, 7, 6),
	(10, 10, 7, 1),
	(11, 11, 7, 7),
	(12, 12, 7, 8),
	(13, 13, 7, 9);
/*!40000 ALTER TABLE `course_uni` ENABLE KEYS */;

-- Dumping structure for table zao.department
DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `department_id` int(11) NOT NULL,
  `department` varchar(15) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.department: ~9 rows (approximately)
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT IGNORE INTO `department` (`department_id`, `department`) VALUES
	(1, 'Agriculter'),
	(2, 'Art'),
	(3, 'Comm'),
	(4, 'Comp'),
	(5, 'Design'),
	(6, 'Development'),
	(7, 'Diploma'),
	(8, 'Mgmt'),
	(9, 'Pharmacy');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;

-- Dumping structure for table zao.language_of_teaching
DROP TABLE IF EXISTS `language_of_teaching`;
CREATE TABLE IF NOT EXISTS `language_of_teaching` (
  `language_id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.language_of_teaching: ~2 rows (approximately)
/*!40000 ALTER TABLE `language_of_teaching` DISABLE KEYS */;
INSERT IGNORE INTO `language_of_teaching` (`language_id`, `name`) VALUES
	(1, 'English'),
	(2, 'Spanish');
/*!40000 ALTER TABLE `language_of_teaching` ENABLE KEYS */;

-- Dumping structure for table zao.living_expenses
DROP TABLE IF EXISTS `living_expenses`;
CREATE TABLE IF NOT EXISTS `living_expenses` (
  `living_expenses_id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `living_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`living_expenses_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.living_expenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `living_expenses` DISABLE KEYS */;
INSERT IGNORE INTO `living_expenses` (`living_expenses_id`, `amount`, `living_year`) VALUES
	(1, 21000, '2019');
/*!40000 ALTER TABLE `living_expenses` ENABLE KEYS */;

-- Dumping structure for table zao.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` int(11) NOT NULL,
  `modules` varchar(10) NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.modules: ~2 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT IGNORE INTO `modules` (`module_id`, `modules`) VALUES
	(1, 'Semester'),
	(2, 'Term');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table zao.origincountry
DROP TABLE IF EXISTS `origincountry`;
CREATE TABLE IF NOT EXISTS `origincountry` (
  `origincountry_id` int(11) NOT NULL,
  `country` varchar(25) NOT NULL,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`origincountry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.origincountry: ~11 rows (approximately)
/*!40000 ALTER TABLE `origincountry` DISABLE KEYS */;
INSERT IGNORE INTO `origincountry` (`origincountry_id`, `country`, `code`) VALUES
	(1, 'India', 'IN'),
	(2, 'Pakistan', 'PK'),
	(3, 'Bangladesh', 'BD'),
	(4, 'Nepal', 'NP'),
	(5, 'Sri Lanka', 'LK'),
	(6, 'UAE', 'AE'),
	(7, 'Philippines', 'PH'),
	(8, 'Vietnam', 'VN'),
	(9, 'Thailand', 'TH'),
	(10, 'United States', 'US'),
	(11, 'United Kingdom', 'UK');
/*!40000 ALTER TABLE `origincountry` ENABLE KEYS */;

-- Dumping structure for table zao.qualification_criteria
DROP TABLE IF EXISTS `qualification_criteria`;
CREATE TABLE IF NOT EXISTS `qualification_criteria` (
  `qualification_criteria_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_subject` varchar(15) NOT NULL,
  `level_id` int(11) NOT NULL,
  `marks` int(11) DEFAULT NULL,
  PRIMARY KEY (`qualification_criteria_id`),
  KEY `fk_test_qualification_criteria` (`level_id`),
  CONSTRAINT `fk_test_qualification_criteria` FOREIGN KEY (`level_id`) REFERENCES `qualification_level` (`level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.qualification_criteria: ~0 rows (approximately)
/*!40000 ALTER TABLE `qualification_criteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualification_criteria` ENABLE KEYS */;

-- Dumping structure for table zao.qualification_level
DROP TABLE IF EXISTS `qualification_level`;
CREATE TABLE IF NOT EXISTS `qualification_level` (
  `level_id` int(11) NOT NULL,
  `qualification` varchar(15) NOT NULL,
  `qualification_value` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.qualification_level: ~2 rows (approximately)
/*!40000 ALTER TABLE `qualification_level` DISABLE KEYS */;
INSERT IGNORE INTO `qualification_level` (`level_id`, `qualification`, `qualification_value`) VALUES
	(1, 'UG', 'Under Graduate'),
	(2, 'PG', 'Post Graduate');
/*!40000 ALTER TABLE `qualification_level` ENABLE KEYS */;

-- Dumping structure for table zao.states
DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(20) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`state_id`),
  KEY `fk_country_states` (`country_id`),
  CONSTRAINT `fk_country_states` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.states: ~7 rows (approximately)
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT IGNORE INTO `states` (`state_id`, `state_name`, `country_id`) VALUES
	(1, 'Gujarat', 3),
	(2, 'Maharastra', 3),
	(3, 'UKS2', 2),
	(4, 'New York', 1),
	(5, 'UKS1', 2),
	(6, 'LA', 1),
	(7, 'California', 1);
/*!40000 ALTER TABLE `states` ENABLE KEYS */;

-- Dumping structure for table zao.tests
DROP TABLE IF EXISTS `tests`;
CREATE TABLE IF NOT EXISTS `tests` (
  `test_id` int(11) NOT NULL,
  `test` varchar(15) NOT NULL,
  `test_type` varchar(15) NOT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.tests: ~7 rows (approximately)
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
INSERT IGNORE INTO `tests` (`test_id`, `test`, `test_type`) VALUES
	(1, 'PTE', 'language'),
	(2, 'TOEFL', 'language'),
	(3, 'IELTS', 'language'),
	(4, 'GRE', 'academic'),
	(5, 'SAT', 'academic'),
	(6, 'ACT', 'academic'),
	(7, 'GMAT', 'academic');
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;

-- Dumping structure for table zao.test_criteria
DROP TABLE IF EXISTS `test_criteria`;
CREATE TABLE IF NOT EXISTS `test_criteria` (
  `test_criteria_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_subject` varchar(100) NOT NULL,
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`test_criteria_id`),
  KEY `fk_test_test_criteria` (`test_id`),
  CONSTRAINT `fk_test_test_criteria` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Dumping data for table zao.test_criteria: ~21 rows (approximately)
/*!40000 ALTER TABLE `test_criteria` DISABLE KEYS */;
INSERT IGNORE INTO `test_criteria` (`test_criteria_id`, `test_subject`, `test_id`) VALUES
	(1, 'overall_score', 4),
	(2, 'verbal', 4),
	(3, 'quantitative', 4),
	(4, 'overall_score', 7),
	(5, 'verbal', 7),
	(6, 'quantitative', 7),
	(7, 'analytical_writing', 7),
	(8, 'reasoning', 7),
	(9, 'language_overall_score', 3),
	(10, 'listening', 3),
	(11, 'reading', 3),
	(12, 'writing', 3),
	(13, 'speaking', 3),
	(14, 'overall_score', 5),
	(15, 'evidence_read_write', 5),
	(16, 'academy_math', 5),
	(17, 'overall_score', 6),
	(18, 'act_english', 6),
	(19, 'academy_math', 6),
	(20, 'act_reading', 6),
	(21, 'act_science', 6);
/*!40000 ALTER TABLE `test_criteria` ENABLE KEYS */;

-- Dumping structure for table zao.university
DROP TABLE IF EXISTS `university`;
CREATE TABLE IF NOT EXISTS `university` (
  `uni_id` int(11) NOT NULL,
  `uni_name` varchar(20) NOT NULL,
  `logo_url` varchar(90) DEFAULT NULL,
  `about` varchar(99) DEFAULT NULL,
  `establisment_year` year(4) DEFAULT NULL,
  `facility` tinyint(1) DEFAULT NULL,
  `sports` varchar(90) DEFAULT NULL,
  `accomodation_provided` tinyint(1) DEFAULT NULL,
  `leisure` tinyint(1) DEFAULT NULL,
  `reading_club` tinyint(1) DEFAULT NULL,
  `digital_labs` tinyint(1) DEFAULT NULL,
  `art_culture_studios` tinyint(1) DEFAULT NULL,
  `student_clubs` tinyint(1) DEFAULT NULL,
  `events_festivals` tinyint(1) DEFAULT NULL,
  `other` varchar(40) DEFAULT NULL,
  `avg_class_size` int(11) DEFAULT NULL,
  `job_support` tinyint(1) DEFAULT NULL,
  `internships` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`uni_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.university: ~7 rows (approximately)
/*!40000 ALTER TABLE `university` DISABLE KEYS */;
INSERT IGNORE INTO `university` (`uni_id`, `uni_name`, `logo_url`, `about`, `establisment_year`, `facility`, `sports`, `accomodation_provided`, `leisure`, `reading_club`, `digital_labs`, `art_culture_studios`, `student_clubs`, `events_festivals`, `other`, `avg_class_size`, `job_support`, `internships`) VALUES
	(1, 'UNI A', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'sss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1),
	(2, 'UNI U', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'ss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1),
	(3, 'UNI S', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'sss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1),
	(4, 'UNI UK', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'ss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1),
	(5, 'UNI K', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'ss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1),
	(6, 'UNI I', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'ss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1),
	(7, 'UNI N', 'http://13.233.145.60:81/ZAO/images/university_of_california.png', 'ss', '2019', 2, '1', 1, 1, 1, 1, 1, 1, 1, '1', 1, 1, 1);
/*!40000 ALTER TABLE `university` ENABLE KEYS */;

-- Dumping structure for table zao.uni_usp
DROP TABLE IF EXISTS `uni_usp`;
CREATE TABLE IF NOT EXISTS `uni_usp` (
  `usp_id` int(11) NOT NULL,
  `uni_id` int(11) NOT NULL,
  `uniqu_selling_points` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`usp_id`),
  KEY `fk_uni_uni_usp` (`uni_id`),
  CONSTRAINT `fk_uni_uni_usp` FOREIGN KEY (`uni_id`) REFERENCES `university` (`uni_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.uni_usp: ~0 rows (approximately)
/*!40000 ALTER TABLE `uni_usp` DISABLE KEYS */;
/*!40000 ALTER TABLE `uni_usp` ENABLE KEYS */;

-- Dumping structure for table zao.uni_yearly
DROP TABLE IF EXISTS `uni_yearly`;
CREATE TABLE IF NOT EXISTS `uni_yearly` (
  `uni_yearly_id` int(11) NOT NULL,
  `uni_id` int(11) NOT NULL,
  `placement_ratio` int(11) DEFAULT NULL,
  `global_ranking` int(11) DEFAULT NULL,
  `local_ranking` int(11) DEFAULT NULL,
  `international_student_share` int(11) DEFAULT NULL,
  `total_students` int(11) DEFAULT NULL,
  `scholarship` tinyint(1) DEFAULT NULL,
  `avg_tution_fee` int(11) DEFAULT NULL,
  `uni_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`uni_yearly_id`),
  KEY `fk_university_yearly` (`uni_id`),
  CONSTRAINT `fk_university_yearly` FOREIGN KEY (`uni_id`) REFERENCES `university` (`uni_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.uni_yearly: ~7 rows (approximately)
/*!40000 ALTER TABLE `uni_yearly` DISABLE KEYS */;
INSERT IGNORE INTO `uni_yearly` (`uni_yearly_id`, `uni_id`, `placement_ratio`, `global_ranking`, `local_ranking`, `international_student_share`, `total_students`, `scholarship`, `avg_tution_fee`, `uni_year`) VALUES
	(1, 1, 91, 1, 7, 12, 23, 1, 1213, '2019'),
	(2, 2, 12, 5, 6, 13, 33, 0, 2131, '2019'),
	(3, 3, 73, 2, 5, 14, 21, 1, 3242, '2019'),
	(4, 4, 45, 3, 4, 15, 50, 0, 4353, '2019'),
	(5, 5, 78, 11, 3, 16, 54, 1, 4353, '2019'),
	(6, 6, 78, 4, 2, 17, 55, 0, 546, '2019'),
	(7, 7, 65, 8, 1, 18, 56, 1, 1875, '2019');
/*!40000 ALTER TABLE `uni_yearly` ENABLE KEYS */;

-- Dumping structure for table zao.visa_success_rate
DROP TABLE IF EXISTS `visa_success_rate`;
CREATE TABLE IF NOT EXISTS `visa_success_rate` (
  `visa_success_id` int(11) NOT NULL,
  `percentage` int(11) DEFAULT NULL,
  `visa_success_rate_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`visa_success_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table zao.visa_success_rate: ~0 rows (approximately)
/*!40000 ALTER TABLE `visa_success_rate` DISABLE KEYS */;
INSERT IGNORE INTO `visa_success_rate` (`visa_success_id`, `percentage`, `visa_success_rate_year`) VALUES
	(1, 70, '2019');
/*!40000 ALTER TABLE `visa_success_rate` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
