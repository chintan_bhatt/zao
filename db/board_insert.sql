INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('16', 'India-CBSE');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('17', 'India-School');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('12', 'NZ');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('10', 'UK & IR');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('9', 'Aus');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('8', 'Canada');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('7', 'USA');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('6', 'GCSE');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('1', 'International Baccalaureate');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('2', 'Irish level');
INSERT INTO `zao`.`board` (`board_id`, `board`) VALUES ('3', 'American Board');

-- Language test insert

INSERT INTO `zao`.`language_test` (`language_id`, `test`) VALUES ('1', 'PTE');
INSERT INTO `zao`.`language_test` (`language_id`, `test`) VALUES ('2', 'TOEFL');
INSERT INTO `zao`.`language_test` (`language_id`, `test`) VALUES ('3', 'IELTS');

-- academic test insert
INSERT INTO `zao`.`entrance_test` (`et_id`, `etest`) VALUES ('1', 'GRE');
INSERT INTO `zao`.`entrance_test` (`et_id`, `etest`) VALUES ('2', 'GMAT');
INSERT INTO `zao`.`entrance_test` (`et_id`, `etest`) VALUES ('3', 'SAT');
INSERT INTO `zao`.`entrance_test` (`et_id`, `etest`) VALUES ('4', 'ACT');

-- create table language of teaching
CREATE TABLE `zao`.`language_of_teaching` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

-- insert
INSERT INTO `zao`.`language_of_teaching` (`id`, `name`) VALUES ('1', 'English');
INSERT INTO `zao`.`language_of_teaching` (`id`, `name`) VALUES ('2', 'Spanish');

--  insert module
INSERT INTO `zao`.`module` (`module_id`, `module`) VALUES ('1', 'semester');
INSERT INTO `zao`.`module` (`module_id`, `module`) VALUES ('2', 'term');

-- insert city
INSERT INTO `zao`.`city` (`city_id`, `city`, `state_id`) VALUES ('1', 'New York', '1');
INSERT INTO `zao`.`city` (`city_id`, `city`, `state_id`) VALUES ('2', 'Chicago', '1');
INSERT INTO `zao`.`city` (`city_id`, `city`, `state_id`) VALUES ('3', 'Los Angeles', '1');
INSERT INTO `zao`.`city` (`city_id`, `city`, `state_id`) VALUES ('4', 'London', '1');

-- insert state
INSERT INTO `zao`.`state` (`state_id`, `state`, `country_id`) VALUES ('1', 'Loondon', '4');

-- insert country
INSERT INTO `zao`.`country` (`country_id`, `country`) VALUES ('1', 'India');
INSERT INTO `zao`.`country` (`country_id`, `country`) VALUES ('2', 'Unites States of America');
INSERT INTO `zao`.`country` (`country_id`, `country`) VALUES ('3', 'Canada');
INSERT INTO `zao`.`country` (`country_id`, `country`) VALUES ('4', 'United Kingdom');

-- add country
ALTER TABLE `zao`.`university` 
ADD COLUMN `country_id` INT NOT NULL AFTER `university`;

-- insert university
INSERT INTO `zao`.`university` (`uni_id`, `university`, `country_id`) VALUES ('1', 'London School Of Commerce', 4);
INSERT INTO `zao`.`university` (`uni_id`, `university`, `country_id`) VALUES ('2', 'Gujarat University', 1);

-- create course level
CREATE TABLE `zao`.`course_level` (
  `id` INT NOT NULL,
  `level` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

-- insert
INSERT INTO `zao`.`course_level` (`id`, `level`) VALUES ('1', 'Diploma');
INSERT INTO `zao`.`course_level` (`id`, `level`) VALUES ('2', 'Bachelor');
INSERT INTO `zao`.`course_level` (`id`, `level`) VALUES ('3', 'Master');
INSERT INTO `zao`.`course_level` (`id`, `level`) VALUES ('4', 'Short');

-- insert countries
INSERT INTO `zao`.`course` (`course_id`, `course`) VALUES ('3', 'Master of Business Administration');
INSERT INTO `zao`.`course` (`course_id`, `course`) VALUES ('1', 'Mechanical Engineer');
INSERT INTO `zao`.`course` (`course_id`, `course`) VALUES ('2', 'Fashion Engineer');
