    -- MySQL Workbench Forward Engineering

    SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
    SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
    SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

    -- -----------------------------------------------------
    -- Schema mydb
    -- -----------------------------------------------------
    DROP SCHEMA IF EXISTS `zao` ;

    -- -----------------------------------------------------
    -- Schema zao
    -- -----------------------------------------------------
    CREATE SCHEMA IF NOT EXISTS `zao` DEFAULT CHARACTER SET utf8 ;
    SHOW WARNINGS;
    -- -----------------------------------------------------
    -- Schema new_schema1
    -- -----------------------------------------------------
    SHOW WARNINGS;
    -- -----------------------------------------------------
    -- Schema new_schema2
    -- -----------------------------------------------------
    SHOW WARNINGS;
    USE `zao` ;

    -- -----------------------------------------------------
    -- Table `zao`.`origincountry`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`origincountry` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`origincountry` (
      `origincountry_id` INT NOT NULL,
      `country` VARCHAR(25) NOT NULL,
      PRIMARY KEY (`origincountry_id`))
    ENGINE = InnoDB;

    SHOW WARNINGS;
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (1, 'India');
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (2, 'Pakistan'); 
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (3, 'Bangladesh');
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (4, 'Nepal'); 
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (5, 'Sri Lanka'); 
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (6, 'UAE'); 
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (7, 'Philippines'); 
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (8, 'Vietnam'); 
    INSERT INTO zao.`origincountry` (`origincountry_id`,`country`) VALUES (9, 'Thailand');
    -- -----------------------------------------------------
    -- Table `zao`.`user`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`user` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`user` (
      `username` VARCHAR(16) NOT NULL,
      `email` VARCHAR(255) NULL,
      `password` VARCHAR(32) NOT NULL,
      `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP);

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`qualification_level`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`qualification_level` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`qualification_level` (
      `level_id` INT NOT NULL,
      `qualification` VARCHAR(25) NOT NULL,
      PRIMARY KEY (`level_id`))
    COMMENT = '	';

    SHOW WARNINGS;
    INSERT INTO zao.`qualification_level` (`level_id`,`qualification`) VALUES (10, '10th');
    INSERT INTO zao.`qualification_level` (`level_id`,`qualification`) VALUES (20, '12th'); 
    INSERT INTO zao.`qualification_level` (`level_id`,`qualification`) VALUES (30, 'Diploma');
    INSERT INTO zao.`qualification_level` (`level_id`,`qualification`) VALUES (31, 'Bachelor'); 
    INSERT INTO zao.`qualification_level` (`level_id`,`qualification`) VALUES (40, 'Master'); 
    
    -- -----------------------------------------------------
    -- Table `zao`.`board`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`board` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`board` (
      `board_id` INT NOT NULL,
      `board` VARCHAR(40) NOT NULL,
      PRIMARY KEY (`board_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`language_test`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`language_test` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`language_test` (
      `language_id` INT NOT NULL,
      `test` VARCHAR(15) NOT NULL,
      PRIMARY KEY (`language_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`entrance_test`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`entrance_test` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`entrance_test` (
      `et_id` INT NOT NULL,
      `etest` VARCHAR(15) NOT NULL,
      PRIMARY KEY (`et_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`city`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`city` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`city` (
      `city_id` INT NOT NULL,
      `city` VARCHAR(25) NOT NULL,
      `state_id` INT NULL,
      PRIMARY KEY (`city_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`modue`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`modue` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`modue` (
      `module_id` INT NOT NULL,
      `module` VARCHAR(10) NOT NULL,
      PRIMARY KEY (`module_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`state`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`state` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`state` (
      `state_id` INT NOT NULL,
      `state` VARCHAR(25) NOT NULL,
      `country_id` INT NULL,
      PRIMARY KEY (`state_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`course_language`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`course_language` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`course_language` (
      `language_id` INT NOT NULL,
      `language` VARCHAR(20) NOT NULL,
      PRIMARY KEY (`language_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`course`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`course` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`course` (
      `course_id` INT NOT NULL,
      `course` VARCHAR(30) NOT NULL,
      PRIMARY KEY (`course_id`))
    COMMENT = '	';

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`country`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`country` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`country` (
      `country_id` INT NOT NULL,
      `country` VARCHAR(25) NOT NULL,
      PRIMARY KEY (`country_id`))
    ENGINE = InnoDB;

    SHOW WARNINGS;

    -- -----------------------------------------------------
    -- Table `zao`.`university`
    -- -----------------------------------------------------
    DROP TABLE IF EXISTS `zao`.`university` ;

    SHOW WARNINGS;
    CREATE TABLE IF NOT EXISTS `zao`.`university` (
      `uni_id` INT NOT NULL,
      `university` VARCHAR(30) NOT NULL,
      PRIMARY KEY (`uni_id`))
    ENGINE = InnoDB;

    SHOW WARNINGS;

    SET SQL_MODE=@OLD_SQL_MODE;
    SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
    SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

